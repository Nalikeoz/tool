from consts import StreamType
from handlers.audio_handler import AudioHandler
from streams.audio_stream import AudioStream
from streams.default_stream import DefaultStream
from streams.exceptions import InvalidStreamTypeException, InvalidStreamIDException
from streams.video_stream import VideoStream


class StreamManager:
    STREAM_TYPE_TO_STREAM_OBJECT = {
        StreamType.AUDIO_STREAM: AudioStream,
        StreamType.VIDEO_STREAM: VideoStream,
        StreamType.DEFAULT_STREAM: DefaultStream
    }

    def __init__(self):
        self.stream_id_to_stream = {}
        self._last_stream_id = 0
        self.audio_handler = AudioHandler()

    def add_stream(self, output_file: str, stream_type: int):
        """
        :param output_file: file to write a stream data to
        :param stream_type: define a stream type.
        Add a new stream.
        """
        if StreamManager.STREAM_TYPE_TO_STREAM_OBJECT.get(stream_type):
            self._last_stream_id += 1
            stream = StreamManager.STREAM_TYPE_TO_STREAM_OBJECT[stream_type](self._last_stream_id, output_file)
            self.stream_id_to_stream[self._last_stream_id] = stream
        else:
            raise InvalidStreamTypeException(f'Invalid stream type: {stream_type}')

    def write_to_stream(self, stream_id, data):
        stream = self.stream_id_to_stream.get(stream_id)
        if stream:
            stream.write(data)
        else:
            raise InvalidStreamIDException(f'Invalid stream ID: {stream_id}')

    def append_to_stream(self, stream_id, data):
        stream = self.stream_id_to_stream.get(stream_id)
        if stream:
            stream.append(data)
        else:
            raise InvalidStreamIDException(f'Invalid stream ID: {stream_id}')

    def get_stream_id_by_file(self, file_name: str):
        """
        :param file_name: a file associated to a stream
        :return: int, stream id that points to `file_name`
        """
        stream_id = [stream_id for (stream_id, stream) in self.stream_id_to_stream.items() if
                     stream.output_file == file_name]
        return stream_id[0] if stream_id else None

    def get_file_by_stream_id(self, stream_id: int):
        """
        :return: a file path that `stream_id` is pointing at
        """
        return self.stream_id_to_stream.get(stream_id).output_file
