from datetime import datetime
from PIL import Image


class Command:
    pass


class CommandResponse:
    pass


class Telemetry:
    """
    Defines a Tool Telemetry.
    """

    def __init__(self, telemetry_type: str, message: str, time: datetime, additional_data: dict = None):
        self.telemetry_type = telemetry_type
        self.message = message
        self.time = time
        self.additional_data = additional_data


class MicrophoneRecord(Command):
    def __init__(self, record_duration: float, stream_id: int):
        """
        :param record_duration: microphone record length in seconds.
        :param stream_id: id of the record stream (handled in handlers/stream_handler)
        """
        self.record_duration = record_duration
        self.stream_id = stream_id


class MicrophoneRecordStop(Command):
    pass


class TerminalCommand(Command):
    def __init__(self, command: str):
        """
        :param command: terminal command to execute
        """
        self.command = command


class CameraShot(Command):
    """
    Sent to the Tool on camera shot command
    """
    pass


class ScreenshotCommand(Command):
    pass


class MicrophoneRecordData(CommandResponse):
    def __init__(self, data_frames: list, stream_id: int):
        """
        :param data_frames: microphone record data
        :param stream_id: id of the record stream (handled in handlers/stream_handler)
        """
        self.data_frames = data_frames
        self.stream_id = stream_id


class TerminalCommandResult(CommandResponse):
    def __init__(self, result: str):
        """
        :param result: output of a terminal command
        """
        self.result = result


class CameraShotData(CommandResponse):
    def __init__(self, return_value: bool, data):
        """
        :param return_value: False is couldn't take image
        :param data: image data
        """
        self.data = data
        self.return_value = return_value


class ScreenshotResult(CommandResponse):
    def __init__(self, pil_image: Image):
        """
        :param pil_image: screenshot image
        """
        self.pil_image = pil_image
