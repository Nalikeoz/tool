class IStream:
    def __init__(self, stream_id, output_file):
        self.stream_id = stream_id
        self.output_file = output_file

    def write(self, data):
        raise NotImplementedError

    def append(self, data):
        raise NotImplementedError