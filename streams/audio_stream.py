from handlers.audio_handler import AudioHandler
from streams.istream import IStream


class AudioStream(IStream):
    def __init__(self, stream_id, output_file):
        super(AudioStream, self).__init__(stream_id, output_file)
        self.audio_handler = AudioHandler()

    def write(self, data_frames):
        self.audio_handler.write_frames_to_file(self.output_file, data_frames)

    def append(self, data_frames):
        self.audio_handler.append_frames_to_file(self.output_file, data_frames)