from streams.istream import IStream


class DefaultStream(IStream):
    def __init__(self, stream_id, output_file):
        super(DefaultStream, self).__init__(stream_id, output_file)

    def write(self, data):
        with open(self.output_file, 'w') as file_handler:
            file_handler.write(data)

    def append(self, data):
        with open(self.output_file, 'a') as file_handler:
            file_handler.write(data)