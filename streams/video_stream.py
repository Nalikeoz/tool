from handlers.video_handler import VideoHandler
from streams.istream import IStream


class VideoStream(IStream):
    def __init__(self, stream_id, output_file):
        super(VideoStream, self).__init__(stream_id, output_file)
        self.video_handler = VideoHandler()

    def write(self, frames):
        self.video_handler.write_frames_to_file(self.output_file, frames)

    def append(self, frames):
        self.video_handler.append_frames_to_file(self.output_file, frames)