class InvalidStreamTypeException(Exception):
    pass


class InvalidStreamIDException(Exception):
    pass
