import wave
import os
import pyaudio
from consts import Directory


class AudioHandler:
    def __init__(self, channels: int = 2, audio_format=pyaudio.paInt16, sample_rate: int = 44100):
        self.channels = channels
        self.audio_format = audio_format
        self.sample_rate = sample_rate
        self.pyaudio = pyaudio.PyAudio()

    def write_frames_to_file(self, output_filename: str, frames: list):
        """
        :param output_filename: name of a file to write data to
        :param frames: sound data frames
        writes sound to a file.
        """
        os.makedirs(os.path.dirname(f'{Directory.AUDIO}{output_filename}'), exist_ok=True)
        wave_object = wave.open(f'{Directory.AUDIO}{output_filename}', 'wb')
        wave_object.setnchannels(self.channels)
        wave_object.setsampwidth(pyaudio.get_sample_size(self.audio_format))
        wave_object.setframerate(self.sample_rate)
        wave_object.writeframes(b''.join(frames))
        wave_object.close()

    def append_frames_to_file(self, output_filename: str, frames: list):
        """
        :param output_filename: name of a file to write data to
        :param frames: sound data frames
        appends sound to a file.
        """
        os.makedirs(os.path.dirname(f'{Directory.AUDIO}{output_filename}'), exist_ok=True)
        if os.path.exists(f'{Directory.AUDIO}{output_filename}'):
            data = []
            wave_object = wave.open(f'{Directory.AUDIO}{output_filename}', 'rb')
            data.append(wave_object.readframes(wave_object.getnframes()))
            wave_object.close()
            frames = data + frames

        wave_object = wave.open(f'{Directory.AUDIO}{output_filename}', 'wb')
        wave_object.setnchannels(self.channels)
        wave_object.setsampwidth(pyaudio.get_sample_size(self.audio_format))
        wave_object.setframerate(self.sample_rate)
        wave_object.writeframes(b''.join(frames))
        wave_object.close()
