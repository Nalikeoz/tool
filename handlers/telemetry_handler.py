DEFAULT_TELEMETRY_FILE = 'telemetries.log'


class TelemetryHandler:
    DATETIME_TELEMETRY_FORMAT = "%d/%m/%Y %H:%M:%S"

    def __init__(self, output_file=DEFAULT_TELEMETRY_FILE):
        """
        :param output_file: str, path to a file to write telemetries in.
        """
        self.output_file = output_file

    def write_telemetry(self, telemetry):
        """
        :param telemetry: Telemetry object
        writes a telemetry to the telemetries file.
        """
        with open(self.output_file, 'a') as telemetry_file:
            telemetry_file.write(
                f'{telemetry.time.strftime(TelemetryHandler.DATETIME_TELEMETRY_FORMAT)} - {telemetry.telemetry_type} - '
                f'{telemetry.message} {telemetry.additional_data if telemetry.additional_data else ""}\n')
