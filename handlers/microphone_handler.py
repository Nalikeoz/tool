import wave
import pyaudio


class MicrophoneHandler:
    CHUNK = 1024
    FORMAT = pyaudio.paInt16
    CHANNELS = 2
    RATE = 44100

    def __init__(self):
        self.pyaudio_obj = pyaudio.PyAudio()
        self.stream = None
        self.is_open = False

    def open(self):
        """
        the function starts to listen to the microphone input.
        """
        self.stream = self.pyaudio_obj.open(format=MicrophoneHandler.FORMAT,
                                            channels=MicrophoneHandler.CHANNELS,
                                            rate=MicrophoneHandler.RATE,
                                            input=True,
                                            frames_per_buffer=MicrophoneHandler.CHUNK)
        self.is_open = True

    def close(self):
        """
        the function stops listening to the microphone input
        """
        if self.stream:
            self.stream.stop_stream()
            self.stream.close()
            self.is_open = False

    def read(self, seconds):
        """
        :param seconds: length of sound data to record.
        :return: list of binary data representing the microphone input.
        """
        frames = []
        for i in range(0, int(MicrophoneHandler.RATE / MicrophoneHandler.CHUNK * seconds)):
            data = self.stream.read(MicrophoneHandler.CHUNK)
            frames.append(data)
        return frames
