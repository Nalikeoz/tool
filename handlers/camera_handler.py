import cv2


class CameraHandler:
    def get_camera_data(self):
        """
        :return:
            ret: bool, whether you manage to take an image or not
            frame: image of the video frame.
        """
        camera_handle = cv2.VideoCapture(0)
        ret, frame = camera_handle.read()
        self.close(camera_handle)
        return ret, frame

    @staticmethod
    def close(camera_handle):
        """
        :param camera_handle: VideoCapture object.
        closes a camera handle and opencv's windows
        """
        camera_handle.release()
        cv2.destroyAllWindows()
