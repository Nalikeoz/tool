import cv2
from os import path
from consts import Directory


class VideoHandler:
    FOURCC = cv2.VideoWriter_fourcc(*'XVID')
    FPS = 15

    @staticmethod
    def __get_video_frames(output_file):
        cap = cv2.VideoCapture(f'{Directory.VIDEO}{output_file}')
        frames = []
        while cap.isOpened():
            ret, frame = cap.read()
            if ret:
                frames.append(frame)
            else:
                break
        cap.release()
        return frames

    @staticmethod
    def write_frames_to_file(output_file, frames):
        height, width, layers = frames[0].shape
        video_writer = cv2.VideoWriter(f'{Directory.VIDEO}{output_file}', VideoHandler.FOURCC, VideoHandler.FPS, (width, height))
        for frame in frames:
            video_writer.write(frame)
        video_writer.release()

    def append_frames_to_file(self, output_file, frames):
        if path.isfile(f'{Directory.VIDEO}{output_file}'):
            video_frames = self.__get_video_frames(f'{Directory.VIDEO}{output_file}')
            frames = video_frames + frames
        self.write_frames_to_file(f'{Directory.VIDEO}{output_file}', frames)

