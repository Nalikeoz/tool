import cv2
import os
import PIL.ImageGrab
from consts import Directory


class ImageHandler:

    @staticmethod
    def save_camera_image(output_filename, image_data):
        os.makedirs(os.path.dirname(rf'{Directory.CAMERA}{output_filename}'), exist_ok=True)
        cv2.imwrite(rf'{Directory.CAMERA}{output_filename}', image_data)

    @staticmethod
    def save_screenshot(output_filename, pil_image):
        os.makedirs(os.path.dirname(rf'{Directory.SCREENSHOT}{output_filename}'), exist_ok=True)
        pil_image.save(rf'{Directory.SCREENSHOT}{output_filename}', quality=100, subsampling=0)

    @staticmethod
    def take_screenshot():
        return PIL.ImageGrab.grab()
