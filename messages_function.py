import pickle
import socket

LEN_OF_LENGTH = 10


def get_message_content(data_length: int, client_socket: socket.socket):
    """
    :param data_length: length of the received data
    :param client_socket: socket to receive data from
    :return: the expected data content
    """
    content = b''
    while len(content) != data_length:
        content += client_socket.recv(data_length - len(content))
    return content


def get_message_length(client_socket: socket.socket):
    """
    the function receives the length of the data.
    :return: the data length
    """
    message_length = ""
    while len(message_length) != LEN_OF_LENGTH:
        message_length += client_socket.recv(LEN_OF_LENGTH - len(message_length)).decode()
    return int(message_length)


def send_object(obj, client_socket: socket.socket):
    """
    :param obj: any object
    :param client_socket: socket to send a data to
    sends a pickled object over socket
    """
    pickled_object = pickle.dumps(obj)
    send_message(pickled_object, client_socket)


def send_message(data: bytes, client_socket: socket.socket):
    """
    :param data: a data to send over socket
    :param client_socket: socket to send data to
    sends data over socket
    """
    client_socket.send(str(len(data)).zfill(LEN_OF_LENGTH).encode())
    client_socket.send(data)


def receive_message(client_socket: socket.socket):
    """
    :param client_socket: the client to receive the data from.
    :return: the client's data.
    """
    message_length = get_message_length(client_socket)
    message = get_message_content(message_length, client_socket)
    return message
