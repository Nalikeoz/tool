import subprocess

SW_HIDE = 0


def start_hidden_mode():
    info = subprocess.STARTUPINFO()
    info.dwFlags = subprocess.STARTF_USESHOWWINDOW
    info.wShowWindow = SW_HIDE
    subprocess.Popen(rf'python client.py', startupinfo=info)


start_hidden_mode()
