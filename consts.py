class TelemetryType:
    MICROPHONE = 'MICROPHONE'
    TERMINAL_COMMAND = 'TERMINAL'
    INFO = 'INFO'
    CAMERA = 'CAMERA'
    SCREENSHOT = 'SCREENSHOT'


class Directory:
    AUDIO = 'Recordings\\Audio\\'
    VIDEO = 'Recordings\\Video\\'
    CAMERA = 'Images\\Camera\\'
    SCREENSHOT = 'Images\\Screenshot\\'


class StreamType:
    VIDEO_STREAM = 0
    AUDIO_STREAM = 1
    DEFAULT_STREAM = 2
